import smtplib
import ssl
import json
import time
import schedule
import requests
from os.path import exists
from bs4 import BeautifulSoup

# Add to "sudo nano /etc/rc.local":
# (cd /home/pi/projects/page_content_change_notifier; python3 page_content_change_notifier.py)&

def read_file(filename):
    with open(filename) as f:
        return f.read()

def write_file(filename, text):
    with open(filename, 'w') as f:
        f.write(text)

def check_file_equals(filename, text):
    if not exists(filename):
        write_file(filename, text)
        return True
    if read_file(filename) == text:
        return True
    write_file(filename, text)
    return False

def load_json(filename):
    with open(filename) as f:
        return json.load(f)


def send_email(data_filename, message):
# data_filename should contain:
# {
#     "smtp_server": "smtp.mail.ru",
#     "port": 587,
#     "sender_email": "sender@mail.ru",
#     "receiver_email": "receiver@gmail.com",
#     "password": "secreet"
# }
    data = load_json(data_filename)

    context = ssl.create_default_context()
    with smtplib.SMTP(data["smtp_server"], data["port"]) as server:
        server.starttls(context=context)
        server.login(data["sender_email"], data["password"])
        server.sendmail(data["sender_email"], data["receiver_email"], message)


def is_page_content_changed(url):
    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')
    egzamin_info = soup.find_all("div", {"class": "col-xs-12 col-sm-9"})
    if len(egzamin_info):
        return not check_file_equals("change.txt", egzamin_info[0].text)
    return True


zapisy_url = "https://www.sjpik.uni.wroc.pl/pl/art/177"
message = """\
Subject: Change at topic:  "Zapisy na egzamin"
Please check https://www.sjpik.uni.wroc.pl/pl/art/177"""


def check_page_content_changed():
    if (is_page_content_changed(zapisy_url)):
        print("Content is changed")
        send_email("data.json", message)


check_page_content_changed()
schedule.every().hour.do(check_page_content_changed)
while True:
    time.sleep(schedule.idle_seconds())
    schedule.run_pending()
